import argparse
import csv
import datetime

import fpdf

from popcornreports import db
from popcornreports.cmdline import display


def create_filename():
    return f"{datetime.datetime.utcnow().strftime('%Y%m%d%H%M%S')}-delivery_report.pdf"


def main():
    arguments = argparse.ArgumentParser(description="Popcorn Scout Delivery Report")
    arguments.add_argument('--scout-roster', '-sc', type=argparse.FileType('r', encoding='UTF-8'), help="Scout Roster CSV", required=True)
    arguments.add_argument('--undelivered-inventory', '-ui', type=argparse.FileType('r', encoding='UTF-8'), help="Undelivered Inventory Report CSV", required=True)
    arguments.add_argument('--db-file', type=str, help="Database File for debugging", required=False, default=None)
    arguments.add_argument('--output', '-o', type=str, help="Output File", required=False, default=None)

    options = arguments.parse_args()
    scout_roster = csv.reader(options.scout_roster, delimiter=",", quotechar='"')
    undelivered_report = csv.reader(options.undelivered_inventory, delimiter=",", quotechar='"')

    orders_db = db.OrdersDatabase(scout_roster_csv=scout_roster, db_file=options.db_file)

    print('Loading Scout Roster...')
    orders_db.scouts.csv.load()

    print('Loading Undelivered Inventories...')
    orders_db.inventory.csv.load_undelivered(undelivered_report)

    class MyPDF(fpdf.FPDF, fpdf.HTMLMixin):
        pass

    pdf_file = MyPDF()
    for scout in orders_db.inventory.scouts(False):
        scout_data = orders_db.scouts.get(scout['scout_id'])

        pdf_file.add_page()
        pdf_file.set_font('Arial', size=24)
        pdf_file.cell(200, 10, txt=scout_data['name'], ln=1, align='C')
        pdf_file.ln(pdf_file.font_size)

        column_width = 0
        pdf_file.set_font('Arial', size=14)
        html_doc = '<html><body><table width="100%"><tbody>'
        for undelivered_inventory_item in orders_db.inventory.get(scout['scout_id'], False):
            html_doc += f'<tr><td width="25%" align="right">{int(undelivered_inventory_item["quantity"]):3}</td><td width="5%">&nbsp</td>'
            html_doc += f'<td width="70%" align="left">{undelivered_inventory_item["product_name"]}</td></tr>'
        html_doc += "</tbody></table></body></html>"
        pdf_file.write_html(html_doc)
        pdf_file.ln(pdf_file.font_size)
        pdf_file.ln(pdf_file.font_size)

        signature_html_doc = '<html><body><table width="100%"><tbody><tr><td align="right" width="25%">Signature:</td><td align="left" width="40%">_________________________________________________</td><td width="35%">&nbsp;</td></tr></tbody></html>'
        pdf_file.write_html(signature_html_doc)

    output_file = (
        options.output
        if options.output
        else create_filename()
    )
    pdf_file.output(output_file)

    return 0
