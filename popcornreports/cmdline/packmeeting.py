"""
Pack Meeting

- load the scout roster
- load the sales detailed report (YTD)
- take a date range
- find during the date range:
  - newest scout selling
    - if possible, not selling over previous periods
  - highest seller
  - list of active sellers
"""
