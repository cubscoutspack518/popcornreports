import prettytable


def print_inventory_dict(title, inventory_dict, show_order=False):
    keys = sorted(inventory_dict.keys())
    table = prettytable.PrettyTable(border=True, header=False)
    table.field_names = ['Popcorn', 'Quantity', 'To Order']
    table.align['Popcorn'] = 'r'
    table.align['Quantity'] = 'r'
    for k in keys:
        table.add_row([k, inventory_dict[k], '*' if inventory_dict[k] < 0 else ''])
    print(f'{title}:')
    if show_order:
        print(table)
    else:
        print(table.get_string(fields=['Popcorn', 'Quantity']))

def print_dict(title, the_dict, fields, show_order=False, alignments=None, print_fields=None):
    keys = sorted(the_dict.keys())
    table = prettytable.PrettyTable(border=True, header=False)
    table.field_names = fields
    if alignments:
        for k, v in alignments.items():
            table.align[k] = v

    for k in keys:
        table.add_row([k, the_dict[k]])
    print(f'{title}:')
    if not print_fields:
        print(table)
    else:
        print(table.get_string(fields=print_fields))
