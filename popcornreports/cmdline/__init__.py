import argparse
import copy
import csv

import prettytable

from popcornreports import db, exceptions
from popcornreports.cmdline import display


def foobar_add_scout_inventory(orders_db, inventory_csv_reader):
    si_header = next(inventory_csv_reader)

    def reform_scout_name(inventory_scout_name):
        parts = inventory_scout_name.split(',')
        return [
            f'{parts[1]} {parts[0]}',
            f'{parts[1]}  {parts[0]}',
        ]

    for inventory_entry in inventory_csv_reader:
        inventory_dict_entry = orders_db.entry_to_dict(si_header, inventory_entry)
        scout_name = reform_scout_name(
            inventory_dict_entry['Scout Name']
        )
        scouts = [
            scout
            for scout in orders_db.get_scout_by_name(
                scout_name[0]
            )
        ]
        if not scouts:
            scouts = [
                scout
                for scout in orders_db.get_scout_by_name(
                    scout_name[1]
                )
            ]

        if len(scouts) != 1:
            print(f"{inventory_dict_entry['Scout Name']} - {scout_name} mapped to {scouts}")

        scout = scouts[0]
        for k, v in inventory_dict_entry.items():
            if 'Scout' not in k and 'Military' not in k:
                v = float(v) if v else 0
                if not v:
                    continue

                try:
                    product = orders_db.product.get(k)
                except exceptions.NoProductFound:
                    print(f'Failed to find product {k}')
                else:
                    try:
                        orders_db.inventory.add(
                            scout['id'],
                            k,
                            v,
                            k['Status Code'] == 'DELIVERED'
                        )
                    except Exception:
                        print(f'Exception while inserting Scout({scout}), Product({product}), Quantity({v})')
                        raise
