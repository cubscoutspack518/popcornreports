import argparse
import copy
import csv

import prettytable

from popcornreports import db, exceptions
from popcornreports.cmdline import display


def main():
    arguments = argparse.ArgumentParser(description="Popcorn Inventory Calculator")
    arguments.add_argument('--sales-report', '-sr', type=argparse.FileType('r', encoding='UTF-8'), help="Sales Detailed Report CSV", required=True)
    arguments.add_argument('--initial-inventory', '-ii', type=argparse.FileType('r', encoding='UTF-8'), help="Initial and Transfer Inventory", required=False)
    arguments.add_argument('--final-inventory', '-fi', type=argparse.FileType('r', encoding='UTF-8'), help="Final Inventory", required=False)
    arguments.add_argument('--final-order', '-fo', type=argparse.FileType('w', encoding='UTF-8'), help="Final order Report CSV", required=True)

    options = arguments.parse_args()
    sales_report = csv.reader(options.sales_report, delimiter=",", quotechar='"')
    try:
        initial_report = csv.reader(options.initial_inventory, delimiter=",", quotechar='"')
    except Exception as ex:
        print(f'Error loading initial inventory: {ex}')
        initial_report = None

    try:
        final_report = csv.reader(options.final_inventory, delimiter=",", quotechar='"')
    except Exception as ex:
        print(f'Error loading final inventory: {ex}')
        final_report = None

    final_order = csv.writer(options.final_order, delimiter=",", quotechar='"')

    orders_db = db.OrdersDatabase(scout_roster_csv=None, sales_csv=sales_report)
    orders_db.sales.csv.load()

    sales_inventory = orders_db.get_final_order()

    display.print_inventory_dict('Total Sold Inventory', sales_inventory)

    initial_inventory=None
    if initial_report:
        print('Loading Initial & Transferred Inventory:')
        initial_inventory = orders_db.inventory.csv.load_inventory(
            initial_report,
            sales_inventory
        )

    final_inventory=None
    if final_report:
        print('Loading Final Inventory:')
        final_inventory = orders_db.inventory.csv.load_inventory(
            final_report,
            sales_inventory
        )

    if initial_inventory:
    # Calculate Final Inventory - first copy the initial inventory
    sales_completion_inventory = copy.deepcopy(initial_inventory)
    display.print_inventory_dict('Inventory For Sales Completion', sales_completion_inventory)

    # subtract out the sold
    print('Adjusting for sales:')
    sales_log = prettytable.PrettyTable(border=False, header=False)
    sales_log.field_names = ['Popcorn', 'Quantity', 'Sold', 'New Quantity', 'Math'] 
    for field in sales_log.field_names:
        sales_log.align[field] = 'r'
    for k, v in sales_inventory.items():
        if k not in sales_completion_inventory:
            sales_completion_inventory[k] = 0

        sales_log.add_row(
            [
                k,
                sales_completion_inventory[k],
                v,
                sales_completion_inventory[k] - v,
                f"{sales_completion_inventory[k]} - {v} = {sales_completion_inventory[k] - v}"
            ]
        )
        sales_completion_inventory[k] -= v
    print(sales_log)
    display.print_inventory_dict('Sales Completion Inventory', sales_completion_inventory)

    display.print_inventory_dict('Final Inventory', final_inventory)
    for k, v in sales_completion_inventory.items():
        if k not in final_inventory:
            final_inventory[k] = 0
        final_inventory[k] += v

    display.print_inventory_dict('Final Inventory', final_inventory, show_order=True)

    return 0
