import argparse
import copy
import csv
import sys

import prettytable

from popcornreports import db, exceptions
from popcornreports.cmdline import display


def main():
    arguments = argparse.ArgumentParser(description="Popcorn Inventory Calculator")
    arguments.add_argument('--sales-report', '-sr', type=argparse.FileType('r', encoding='UTF-8'), help="Sales Detailed Report CSV", required=True)
    arguments.add_argument('--military-order', '-mo', type=argparse.FileType('w', encoding='UTF-8'), help="Military Report CSV", required=True)
    arguments.add_argument('--db-file', type=str, help="Database File for debugging", required=False, default=None)

    options = arguments.parse_args()
    sales_report = csv.reader(options.sales_report, delimiter=",", quotechar='"')
    military_order = csv.writer(options.military_order, delimiter=",", quotechar='"')

    orders_db = db.OrdersDatabase(scout_roster_csv=None, sales_csv=sales_report, db_file=options.db_file)
    orders_db.sales.csv.load()

    per_scout_military_order = {}

    DONATIONS = {
        "line_item": {
            #"$1 Military Donation".lower(): 1.00,
            #"Misc. Military $".lower(): 1.00,
            #"A Soldier's Salute Donation".lower(): 25.00,
            #"Courage Military Donation".lower(): 50.00,
            #"Gold Level Military Donation".lower(): 50.00,
            #"Silver Level Military Donation".lower(): 30.00,
            #"Stars & Stripes Level Military Donation".lower(): 100.00,

            "$1 American Heroes Donation".lower(): 1.00,
            "$30 American Heroes Donation".lower(): 30.00,
            "$50 American Heroes Donation".lower(): 50.00,
            "$100 American Heroes Donation".lower(): 100.00,
            "$250 American Heroes Donation".lower(): 250.00,
            "Gold Level American Heroes Donation".lower(): 50.00,
        },
        "bundled": {
            #"Joint Forces Bundle".lower(): 50.00,
            #"All American Bundle".lower(): 40.00,
        }
    }
    MILITARY_DONATIONS = DONATIONS['line_item'].keys()
    BUNDLE_DONATIONS = DONATIONS['bundled'].keys()

    for scout in orders_db.scouts.get_all():
        per_scout_military_order[scout['name']] = {}
        for scout_sale in orders_db.inventory.get_scout_order(scout['id']):
            name = scout_sale['product_name'].lower()
            if name in MILITARY_DONATIONS:
                if name not in per_scout_military_order[scout['name']]:
                    per_scout_military_order[scout['name']][name] = 0.0

                per_scout_military_order[scout['name']][name] += scout_sale['quantity']
            elif "donation" in name:
                print("Found donation item uncounted: {0}".format(name))
                sys.exit(1)
            elif name in BUNDLE_DONATIONS:
                if name not in per_scout_military_order[scout['name']]:
                    per_scout_military_order[scout['name']][name] = 0.0

                per_scout_military_order[scout['name']][name] += scout_sale['quantity']

    per_scout_totals = {}
    for scout_name, scout_donations in per_scout_military_order.items():
        if scout_donations:
            if scout_name not in per_scout_totals:
                per_scout_totals[scout_name] = {
                    'patch': 0,
                    'pins': 0,
                    'donations': 0.00
                }

            for product_name, quantity in scout_donations.items():
                if product_name in MILITARY_DONATIONS:
                    per_scout_totals[scout_name]['donations'] += DONATIONS['line_item'][product_name] * quantity
                elif product_name in BUNDLE_DONATIONS:
                    per_scout_totals[scout_name]['donations'] += DONATIONS['bundled'][product_name] * quantity

    for scout_name in per_scout_totals.keys():
        donation = per_scout_totals[scout_name]['donations']
        if donation >= 50.00:
            # first $50 gets a patch
            per_scout_totals[scout_name]['patch'] = 1
            donation -= 50.00

            # there's a pin for every $50 beyond the patch
            per_scout_totals[scout_name]['pins'] = (
                int(donation/50.00)
            )
        else:
            print(f"{scout_name} only had {donation} and does not qualify for Patch or Pins")

    military_order.writerow(['Scout', 'Patch', 'Pins'])
    military_order_data = {'pins': 0, 'patches': 0}
    for scout_name, info in per_scout_totals.items():
        try:
            military_order_data['patches'] += info['patch']
            military_order_data['pins'] += info['pins']
            military_order.writerow([scout_name, info['patch'], info['pins']])
            print(f"{scout_name:20} - Patch: {info['patch']} - Pins: {info['pins']}")
        except KeyError:
            print(f"Error: {scout_name} - {info}")

    print(f"Totals: Patches: {military_order_data['patches']} - Pins: {military_order_data['pins']}")
    military_order.writerow(['Grand Totals',])
    military_order.writerow(['Patches', military_order_data['patches']])
    military_order.writerow(['Pins', military_order_data['pins']])

    return 0

