import argparse
import csv

from popcornreports import db
from popcornreports.cmdline import display


def main():
    arguments = argparse.ArgumentParser(description="Popcorn Inventory Calculator")
    arguments.add_argument('--sales-report', '-sr', type=argparse.FileType('r', encoding='UTF-8'), help="Sales Detailed Report CSV", required=True)
    arguments.add_argument('--scout-roster', '-sc', type=argparse.FileType('r', encoding='UTF-8'), help="Scout Roster CSV", required=True)
    arguments.add_argument('--award-config', '-ac', type=argparse.FileType('r', encoding='UTF-8'), help="Award JSON Configuration", required=True)

    options = arguments.parse_args()
    sales_report = csv.reader(options.sales_report, delimiter=",", quotechar='"')
    scout_roster = csv.reader(options.scout_roster, delimiter=",", quotechar='"')
    award_report = json.load(options.award_config)
    """
    Example Award Report:
        {
            1650.0: {
                "description": "Two big trips",
                "scouts": []
            },
            575.0: {
                "description": "One big trip",
                "scouts": []
            },
            375.0: {
                "description": "Half big trip",
                "scouts": []
            }
        }
    """

    orders_db = db.OrdersDatabase(scout_roster_csv=scout_roster, sales_csv=sales_report)

    print('Loading Scout Roster...')
    orders_db.scouts.csv.load()

    print('Loading Sales Transactions...')
    orders_db.sales.csv.load()

    print('Calculating Statistics...')
    scout_sales = {}
    rank_sales = {}
    running_total = 0.0
    for scout in orders_db.scouts.get_all():
        if not scout['rank']:
            print(f'Skipping Scout without Rank - {scout}')
            continue

        if scout['name'] not in scout_sales:
            scout_sales[scout['name']] = 0.0
        if scout['rank'] not in rank_sales:
            rank_sales[scout['rank']] = 0.0

        for scout_order in orders_db.get_scout_orders(scout['id']):
            print(scout_order)
            scout_sales[scout['name']] += scout_order['total_value']
            rank_sales[scout['rank']] += scout_order['total_value']
            running_total += scout_order['total_value']

    print(f'Total Sales: {running_total}')
    display.print_dict('Ranks', rank_sales, ['Rank', 'Sales'], alignments={'Rank': 'r', 'Sales': 'r'})

    reward_keys = sorted(award_report.keys())
    reward_keys.reverse()
    for k, v in scout_sales.items():
        for rk in reward_keys:
            if v >= rk:
                if 'scouts' not in award_report[rk]:
                    award_report[rk]['scouts'].append(k)

    for ignored, v in award_report:
        print(v['description'])
        for scout in v['scouts']:
            print(f'\t{scout}')

    return 0
