import copy
import sqlite3

from popcornreports import exceptions

from popcornreports.db import (
    inventory,
    product,
    sales,
    scouts,
)

"""
"Order Number","Scout","Scout Code","Product Name","Quantity",
"Total Line Item Amount","Payment Type","Sale Type",
"Date Taken","Status Code","Site Name: Date","Site Address Line 1","Site City","Shift","Customer Name",
"Customer Address Line 1","Customer Address Line 2","Customer City","Customer Zip","Customer Phone","Customer Email"
"""
SCHEMAS = [
"""
CREATE TABLE scouts (
    id INTEGER UNIQUE PRIMARY KEY,
    name TEXT NOT NULL,
    scout_code TEXT NOT NULL,
    den TEXT,
    rank TEXT
)
""",
"""
CREATE TABLE products (
    id INTEGER UNIQUE PRIMARY KEY,
    name TEXT NOT NULL
)
""",
"""
CREATE TABLE orders (
    id INTEGER UNIQUE PRIMARY KEY,
    order_name TEXT,
    scout_id INTEGER,
    product_id INTEGER,
    quantity REAL,
    line_item_total INTEGER,
    payment_type TEXT,
    sale_type TEXT,
    te_datetime TEXT,
    status_code TEXT
)
""",
"""
CREATE TABLE scout_inventory(
    id INTEGER UNIQUE PRIMARY KEY,
    scout_id INTEGER,
    product_id INTEGER,
    quantity REAL,
    delivered INTEGER
)
""",
]

SQL_GET_FINAL_ORDER = "SELECT products.name AS product_name, orders.quantity AS quantity FROM orders, products WHERE orders.sale_type = 'Wagon' AND orders.product_id = products.id"

SQL_INSERT_ORDER = """
INSERT INTO orders (order_name, scout_id, product_id, quantity, line_item_total, payment_type, sale_type, te_datetime, status_code)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
"""
SQL_GET_ORDER_BY_SCOUT_ID = "SELECT line_item_total FROM orders WHERE scout_id = ?"

# From Python documentation:
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


class OrdersDatabase(object):

    @staticmethod
    def entry_to_dict(header, entry):
        return {
            header[i]: entry[i]
            for i in range(len(header))
        }

    def __init__(self, scout_roster_csv=None, sales_csv=None, db_file=None):
        self.db = sqlite3.connect(
            db_file
            if db_file
            else ':memory:'
        )
        self.initialize()
        self.scouts = scouts.ScoutManager(
            self.db,
            self,
            scout_roster_csv
        )
        self.sales = sales.SalesManager(
            self.db,
            self,
            sales_csv
        )
        self.product = product.ProductManager(
            self.db,
            self,
            None  # Product doesn't auto-load
        )
        self.inventory = inventory.InventoryManager(
            self.db,
            self,
            None  # Inventory doesn't auto-load
        )


    def initialize(self):
        cursor = self.db.cursor()
        for s in SCHEMAS:
            cursor.execute(s)
        self.db.commit()

    def add_order(self, order_dict, scout, product):
        with self.db as cursor:
            cursor.execute(
                SQL_INSERT_ORDER,
                (
                    order_dict['Order Number'],
                    scout['id'],
                    product['id'],
                    order_dict['Quantity'],
                    order_dict['Total Line Item Amount'],
                    order_dict['Payment Type'],
                    order_dict['Sale Type'],
                    order_dict['Date Taken'],
                    order_dict['Status Code'],
                )
            )

    def get_final_order(self):
        final_order = {}
        
        cursor = self.db.cursor()
        result = cursor.execute(SQL_GET_FINAL_ORDER)
        for order_result_tuple in result.fetchall():
            order_result = {
                'product_name': order_result_tuple[0],
                'quantity': order_result_tuple[1]
            }
            if not order_result['product_name'] in final_order:
                final_order[order_result['product_name']] = 0 

            final_order[order_result['product_name']] += order_result['quantity']

        return final_order

    def get_scout_orders(self, scout_id):
        cursor = self.db.cursor()

        result = cursor.execute(SQL_GET_ORDER_BY_SCOUT_ID, (scout_id,))
        for row in result.fetchall():
            yield {
                'total_value': row[0],
            }

    def get_all_scout_donations(self):
        cursor = self.db.cursor()

        for scout in self.scouts.get_all():
            scout_donations = {}
            scout_donations.update(scout)
            scout_donations['donations'] = [
                donation_record
                for donation_record in self.inventory.get_military_order(scout['id'])
            ]
            yield scout_donations
