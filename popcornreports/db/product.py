from popcornreports import exceptions
from popcornreports.db import base


SQL_GET_PRODUCT_BY_NAME = "SELECT id, name FROM products WHERE name = ?"
SQL_INSERT_PRODUCT = "INSERT INTO products(name) VALUES(?)"


class ProductManagerCsv(base.BaseCsvInterface):
    pass


class ProductManager(base.BaseDbSubManager):

    def __init__(self, db, parent, csv_interface):
        super(ProductManager, self).__init__(db, parent)
        self.csv = ProductManagerCsv(csv_interface, db, parent)

    def add(self, product_name):
        with self.db as cursor:
            cursor.execute(SQL_INSERT_PRODUCT, (product_name, ))
        return self.get(product_name)

    def get(self, product_name):
        cursor = self.db.cursor()
        result = cursor.execute(SQL_GET_PRODUCT_BY_NAME, (product_name,))
        row = result.fetchone()
        if row:
            return {
                'id': row[0],
                'name': row[1],
            }
        else:
            raise exceptions.NoProductFound
