from popcornreports import exceptions
from popcornreports.db import base


class SalesManagerCsv(base.BaseCsvInterface):

    def __init__(self, csv_interface, db, parent):
        super(SalesManagerCsv, self).__init__(csv_interface, db, parent)

    def add_order(self, csv_data):
        try:
            scout = self.parent.scouts.get(csv_data['Scout Code'])
        except exceptions.NoScoutFound:
            scout = self.parent.scouts.add(
                csv_data['Scout'],
                csv_data['Scout Code']
            )
        try:
            product = self.parent.product.get(csv_data['Product Name'])
        except exceptions.NoProductFound:
            product = self.parent.product.add(csv_data['Product Name'])

        self.parent.add_order(csv_data, scout, product)

    def load(self):
        for entry in self.csv_interface:
            self.add_order(
                self.parent.entry_to_dict(self.header, entry)
            )


class SalesManager(base.BaseDbSubManager):

    def __init__(self, db, parent, csv_interface):
        super(SalesManager, self).__init__(db, parent)
        self.csv = SalesManagerCsv(csv_interface, db, parent)
