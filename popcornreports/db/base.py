"""
"""


class BaseDbSubManager(object):

    def __init__(self, db, parent):
        self.db = db
        self.parent = parent


class BaseCsvInterface(BaseDbSubManager):

    def __init__(self, csv_interface, db, parent):
        super(BaseCsvInterface, self).__init__(db, parent)
        self.csv_interface = csv_interface
        self.header_row = (
            next(self.csv_interface)
            if self.csv_interface
            else {}
        )

    @property
    def header(self):
        return self.header_row
