from popcornreports import exceptions
from popcornreports.db import base

SQL_GET_ALL_SCOUTS = "SELECT id, name, scout_code, den, rank FROM scouts"
SQL_GET_SCOUT_BY_SCOUT_CODE = "SELECT id, name, scout_code, den, rank FROM scouts WHERE scout_code = ?"
SQL_GET_SCOUT_BY_NAME = "SELECT id, name, scout_code, den, rank FROM scouts WHERE name = ?"
SQL_INSERT_SCOUT = "INSERT INTO scouts(name, scout_code) VALUES(?, ?)"
SQL_INSERT_SCOUT_WITH_RANK = "INSERT INTO scouts(name, scout_code, den, rank) VALUES(?, ?, ?, ?)"


class ScoutManagerCsv(base.BaseCsvInterface):

    def __init__(self, csv_interface, db, parent):
        super(ScoutManagerCsv, self).__init__(csv_interface, db, parent)

    def add_with_rank(self, scout):
        return self.parent.scouts.add_with_rank(
            scout['Scout Name'],
            scout['Scout Code'],
            scout['Den #'],
            scout['Rank']
        )

    def load(self):
        for scout in self.csv_interface:
            self.add_with_rank(
                self.parent.entry_to_dict(self.header, scout)
            )


class ScoutManager(base.BaseDbSubManager):

    def __init__(self, db, parent, csv_interface):
        super(ScoutManager, self).__init__(db, parent)
        self.csv = ScoutManagerCsv(csv_interface, db, parent)

    def add(self, scout_name, scout_id):
        with self.db as cursor:
            cursor.execute(SQL_INSERT_SCOUT, (scout_name.strip(), scout_id))
        return self.get(scout_id)

    def add_with_rank(self, scout_name, scout_id, den, rank):
        with self.db as cursor:
            cursor.execute(SQL_INSERT_SCOUT_WITH_RANK, (scout_name.strip(), scout_id, den, rank))

        return self.get(scout_id)

    def get_all(self):
        cursor = self.db.cursor()
        result = cursor.execute(SQL_GET_ALL_SCOUTS)
        for row in result.fetchall():
            yield {
                'id': row[0],
                'name': row[1],
                'scout_code': row[2],
                'den': row[3],
                'rank': row[4],
            }

    def get(self, scout_id):
        cursor = self.db.cursor()
        result = cursor.execute(SQL_GET_SCOUT_BY_SCOUT_CODE, (scout_id,))
        row = result.fetchone()
        if row:
            return {
                'id': row[0],
                'name': row[1],
                'scout_code': row[2],
                'den': row[3],
                'rank': row[4],
            }
        else:
            raise exceptions.NoScoutFound

    def get_by_name(self, name):
        cursor = self.db.cursor()
        result = cursor.execute(SQL_GET_SCOUT_BY_NAME, (name,))
        for row in result.fetchall():
            yield {
                'id': row[0],
                'name': row[1],
                'scout_code': row[2],
                'den': row[3],
                'rank': row[4],
            }
