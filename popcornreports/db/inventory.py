from popcornreports import exceptions
from popcornreports.db import base


SQL_ADD_INVENTORY = "INSERT INTO scout_inventory(scout_id, product_id, quantity, delivered) VALUES(?, ?, ?, ?)"
SQL_GET_INVENTORY = "SELECT products.name AS product_name, scout_inventory.quantity AS quantity FROM scout_inventory, products WHERE scout_inventory.product_id = products.id AND scout_inventory.scout_id = ? AND scout_inventory.delivered = ?"

SQL_GET_SCOUTS_BY_STATUS = "SELECT DISTINCT scout_id FROM scout_inventory WHERE delivered = ?"
SQL_GET_SCOUT_DONATIONS = "SELECT products.name AS product_name, orders.quantity FROM orders, products WHERE orders.product_id = products.id AND orders.scout_id = ?"

SQL_GET_ALL_INVENTORY = "SELECT products.name AS product_name, scout_inventory.quantity AS quantity FROM scout_inventory, products WHERE scout_inventory.product_id = products.id"


class InventoryManagerCsv(base.BaseCsvInterface):

    def __init__(self, csv_interface, db, parent):
        super(InventoryManagerCsv, self).__init__(csv_interface, db, parent)

    def load_inventory(self, inventory_iterable, reference):
        header_row  = next(inventory_iterable)
        loaded_inventory = {}
        for inventory_entry in inventory_iterable:
            inventory_dict_entry = self.parent.entry_to_dict(header_row, inventory_entry)
            for k, v in inventory_dict_entry.items():
                if k in reference:
                    loaded_inventory[k] = float(v)
        return loaded_inventory

    def add_undelivered(self, csv_data):
        try:
            scout = self.parent.scouts.get(csv_data['Scout Code'])
        except exceptions.NoScoutFound:
            scout = self.parent.scouts.add(
                csv_data['Scout Name'],
                csv_data['Scout Code']
            )
        try:
            product = self.parent.product.get(csv_data['Product'])
        except exceptions.NoProductFound:
            self.parent.product.add(csv_data['Product'])

        self.parent.inventory.add(
            scout['scout_code'],
            csv_data['Product'],
            csv_data['Quantity Undelivered'],
            False
        )

    def load_undelivered(self, inventory_iterable):
        header_row = next(inventory_iterable)
        for inventory_entry in inventory_iterable:
            self.add_undelivered(
                self.parent.entry_to_dict(header_row, inventory_entry)
            )


class InventoryManager(base.BaseDbSubManager):

    @staticmethod
    def delivered_status(delivered):
        if delivered:
            return 1
        return 0

    def __init__(self, db, parent, csv_interface):
        super(InventoryManager, self).__init__(db, parent)
        self.csv = InventoryManagerCsv(csv_interface, db, parent)

    def add(self, scout_id, product_name, quantity, delivered):
        product = self.parent.product.get(product_name)
        with self.db as cursor:
            cursor.execute(
                SQL_ADD_INVENTORY,
                (
                    scout_id,
                    product['id'],
                    quantity,
                    self.delivered_status(delivered)
                )
            )

    def get(self, scout_id, delivered):
        cursor = self.db.cursor()
        result = cursor.execute(
            SQL_GET_INVENTORY,
            (
                scout_id,
                self.delivered_status(delivered)
            )
        )
        for inventory_entry in result.fetchall():
            yield {
                'product_name': inventory_entry[0],
                'quantity': inventory_entry[1],
            }

    def all(self):
        cursor = self.db.cursor()
        result = cursor.execute(SQL_GET_ALL_INVENTORY)
        for inventory_entry in result.fetchall():
            yield {
                'product_name': inventory_entry[0],
                'quantity': inventory_entry[1],
            }

    def scouts(self, delivered):
        cursor = self.db.cursor()
        result = cursor.execute(
            SQL_GET_SCOUTS_BY_STATUS,
            (self.delivered_status(delivered), )
        )
        for inventory_entry in result.fetchall():
            yield {
                'scout_id': inventory_entry[0],
            }

    def get_scout_order(self, scout_id):
        cursor = self.db.cursor()
        result = cursor.execute(SQL_GET_SCOUT_DONATIONS, (scout_id,))
        for row in result.fetchall():
            yield {
                'product_name': row[0],
                'quantity': row[1],
            }
