"""
"""


class FinalReportError(Exception):
    pass


class FinalReportDatabaseError(FinalReportError):
    pass


class NoScoutFound(FinalReportDatabaseError):
    pass


class NoProductFound(FinalReportDatabaseError):
    pass
