#!/bin/bash

source venv/bin/activate

SCOUT_ROSTER_CSV="/home/bmeyer/Documents/Personal/orgs/scouts/2020-Popcorn-files/final-order/2020-11-01_Scout_Roster.csv"
SALE_REPORT_CSV="/home/bmeyer/Documents/Personal/orgs/scouts/2020-Popcorn-files/final-order/0000_Sales_Transaction_Detail-c3e2be124a08.csv"
#INITIAL_INVENTORY_CSV="20191102_00_initial-inventory.csv"
#FINAL_INVENTORY_CSV="20191102_01_remaining-inventory.csv"

#SALE_REPORT_CSV="20191103_03_sales-detailed-transaction-report.csv"
#SALE_REPORT_CSV="2019-12-08_Sales_Detailed.csv"
#INITIAL_INVENTORY_CSV="20191102_00_initial-inventory.csv"
#SNS_REPORT_CSV="20191102_shownsell.csv"
#WAGON_REPORT_CSV="20191102_01_pack-wagon-checkouts.csv"
#FINAL_INVENTORY_CSV="20191102_01_remaining-inventory.csv"
#SCOUT_ROSTER_CSV="20191103_00_scout-roster.csv"
UNDELIVERED_ITEMS_REPORT_CSV="2019-11-11_Undelivered_Items_by_Scout.csv"
MILITARY_ORDER_CSV="2019-12-08_Military-Order.csv"
ACTIVE_AWARD_REPORT="configs/pack518_2019_award_config.json"

DB_FILE="--db-file brm.20201101.sqlite"
#DB_FILE="--db-file brm.test2.sqlite"

case "${1}" in

    "order")
        #te_popcorn_reports -sr ${SALE_REPORT_CSV} -fo 00000.csv -ii ${INITIAL_INVENTORY_CSV} -fi ${FINAL_INVENTORY_CSV}
        te_popcorn_reports -sr ${SALE_REPORT_CSV} -fo 00000.csv
        ;;

    "rank")
        te_ranks -sr ${SALE_REPORT_CSV} -sc ${SCOUT_ROSTER_CSV} -ac ${ACTIVE_AWARD_REPORT}
        ;;

    "delivery")
        te_deliveries -sc ${SCOUT_ROSTER_CSV} -ui ${UNDELIVERED_ITEMS_REPORT_CSV} ${DB_FILE}
        ;;
	"military")
		te_military -sr ${SALE_REPORT_CSV} -mo ${MILITARY_ORDER_CSV}
		;;

    "all")
        ${0} order
        ${0} rank
        ${0} delivery
        ;;
    *)
        echo "Help: ${1} [order|rank|delivery]"
        ;;
esac
