Tools For Trail's End Final Order Generation
============================================

Preparing the input data
------------------------
1. Download the Sales Detailed Report from your Trail's End Admin Account
2. Load into Excel or LibreOffice
3. Remove the first big header and delete the empty rows so you're left with the columns of data and the header row right above them
4. Save a copy as CSV

.. note:: If prompted, set the encoding to UTF-8 and quote character to a double-quote ("). Quoting all fields is good too.

Installing the tool
-------------------

This is a python-based tool. As such you need a python environment to run in.
Once Python is setup, just do:

pip install -e .

This will install a tool called `te_popcorn_reports` into your command-line environment.

Getting Help from the tool
--------------------------

.. code-block:: shell
    $ te_popcorn_reports --help

Running
-------

.. code-block:: shell
    $ te_popcorn_reports --sales-report csv-detailed-sales-report-from-trails-end.csv --final-order your-final-order.csv


Using the Final Order CSV 
-------------------------

Load the final order report back into Excel or LibreOffice via either importing the CSV data or opening the CSV file.
